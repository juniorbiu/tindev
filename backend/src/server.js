const express = require('express');
const mongoose = require('mongoose');
const routes = require('./routes');
const cors = require("cors");

//Criando Servidor
const server = express();
//Dizendo para o express utilizar JSON
server.use(express.json());
//Conexão com banco de dados
mongoose.connect(
    'mongodb+srv://omnistack:omnistack@cluster0-kqzm1.mongodb.net/omnistack8?retryWrites=true&w=majority',
    {useNewUrlParser: true, useUnifiedTopology: true}
    );
//Usando o cors para deixar a api aberta
server.use(cors());
//Dizendo para o servidor usar tais rotas
server.use(routes);
//Dizendo para o servidor escutar tal porta
server.listen(3333);